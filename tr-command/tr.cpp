/*Gaurav Agarwal, Roll-20162088*/
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <string.h>
#include <malloc.h>
using namespace std;
char map_table[128];
int flags[5]={0}; /*d,s,c,I,O*/

/*Opens file and returns FD*/
int openfile(char *filename,int mode)
{
	
	int fd;
	int check=access(filename, R_OK);
	if(check==0){
		fd=open(filename,mode);
		if(fd==-1){
			cout<<filename<<": No such file or directory\n";
			return fd;
		}
	}
	else
	{
		cout<<filename<<": No Read permission\n";
		return check;
	}
	
	return fd;
}

/*Creates Directory as well as file if NOT exists*/
int createfile(char* path,int permission)
{
	int i;

	/*Create Directory in Case directory not present*/
	for(i=0;path[i]!='\0';i++)
	{
		if(path[i]=='/')
		{
			
			path[i]='\0';
			
				mkdir(path, S_IRWXU);
				//cout<<"Created";
			
			path[i]='/';
		}
	}
	/*creates file*/
	return creat(path, permission);

}

/*Enables Delete*/
void delete_char(char *characters)
{
	for(int i=0;characters[i]!='\0';i++)
		map_table[characters[i]]='\0';
}

/*Enables Squeeze*/
void squeeze(int rfd,int wfd,char *set)
{
	/*input= file descriptor of source and destination
		rfd,wfd*/
	int i;
	char *cbuff=(char*)malloc(sizeof(char));
	char *lbuff=(char*)malloc(sizeof(char));
	lbuff[0]='\0';
	while(read(rfd,cbuff,1))
	{
		

		if(lbuff[0]!=cbuff[0]){
			
			write(wfd,cbuff,1);
		}
		else
		{
			for(i=0;set[i]!='\0';i++)
				if(cbuff[0]==set[i])
					break;
			if(set[i]!=cbuff[0])
				write(wfd,cbuff,1);

		}
		lbuff[0]=cbuff[0];

	}
	free(cbuff);
	free(lbuff);

}

/*Enables Complementation of a SET*/
void complement(char *eset)
{
	char *complementset=(char*)malloc(128*sizeof(char));
	int k=0;
	int temp;
	for(int i=0;i<128;i++)
		complementset[i]=i;
	
	for(int i=0,k=0;eset[i]!='\0';i++,k++)
	{
		temp=eset[i];
		complementset[temp]='\0';
	}
	
	for(int i=20;i<128;i++)
		if(complementset[i]!='\0'){
			eset[k]=complementset[i];
			k++;
		}
	eset[k]='\0';
	
	free(complementset);
		
		
	return;


}

/*Enables expansion of SERIES*/
void expand(char* from,char* efrom)
{
	int i=0,k=0;
		while(from[i])
		{
			if(from[i]=='-')
			{
				if(from[i+1])
				{
					if(from[i+1]-from[i-1]>-1){
						while(from[i-1]++!=from[i+1])
							efrom[k++]=from[i-1];
							i+=2;
					}
					else{
						cout<<"tr: range-endpoints of '"<<from[i-1]<<from[i]<<from[i+1]<<"' are in reverse collating sequence order\n";
						return;
					}
				}
				else{ 
					efrom[k++]=from[i++];

				}
			}
			else{
				
				efrom[k++]=from[i++];
			
			}
		}
		efrom[k]='\0';
		
}

/*Enables TRANSLATION*/
void map(char *efrom,char* eto)
{
	
	int i=0,k=0;
	while(efrom[i]!='\0'){
		
		
		map_table[efrom[i]]=eto[k];
		i++;
		if(eto[k+1]!='\0')
			k++;
	}
}

/*Enables handling [:lower:] [:upper:]*/
void set_manipulation(char* from)
{
	if(!(strcmp(from,"[:digits:]")))
	{
		strcpy(from,"0-9");
	}
	else if(!strcmp(from,"[:lower:]"))
	{
		strcpy(from,"a-z");
	}
	else if(!strcmp(from,"[:upper:]"))
	{
		strcpy(from,"A-Z");
	}
	else if(!strcmp(from,"[:space:]"))
	{
		strcpy(from," ");
	}
	else if(!strcmp(from,"[:punct:]"))
	{
		strcpy(from,"!#$&'()*+,-./:;<=>?@[^_`{|}~]");
		
	}	
	
}

/*Driver*/
int main(int argc,char* argv[])
{
	char* input_file=NULL;
	char* output_file=NULL;
	int sets; //Counts number of SETS
	int c; //Temperory storage for a character
	int k=0,flag=0,IOflag=0; //k=count of SETS //Flag occurance of a SET in input
	char *buff=(char*)malloc(sizeof(char));	//For READ WRITE to file
	char **set=(char**)malloc(2*sizeof(char*)); //Pointers to point to input set

	/*Initial value for ASCII table*/
	for(int i=0;i<128;i++)
		map_table[i]=i;

	/*Processing of Command line Argument*/
	for(int i=1;i<argc;i++)
	{
		if(argv[i][0]!='-')
        {
                flag=1; //Once I have encountered A SET, no FLAGS can occur
                set[k++]=argv[i];
				continue;
        }

		if(argv[i][0]=='-' && flag==0 || strcmp(argv[i],"-I")==0 || strcmp(argv[i],"-O")==0)
		{
			int j=1;
			while(argv[i][j]!='\0')
			{
				switch(argv[i][j++])
				{
					case 'd': flags[0]=1;
						break;
			 		case 's': flags[1]=1;
	    	                                break;
			 		case 'c': flags[2]=1;
	            	                        break;
			 		case 'I': flags[3]=1;
			 					if(i<=argc-1){
			 						input_file=argv[++i];
			 						IOflag=1;
			 					}
			 					else{
			 						cout<<"tr: Missing input file name after -I\n";
									return 0;
			 					}
	                    	                break;
			 		case 'O': flags[4]=1;
			 					if(i<=argc-1){
			 						output_file=argv[++i];
			 						IOflag=1;
			 					}
			 					else{
			 						cout<<"tr: Missing output_file name after -O\n";
									return 0;
			 					}
	                            	        break;
					default:	cout<<"tr: invalid option -- '"<<argv[i][j-1]<<"'\n";
								return 0;
				}
				if(IOflag==1)
					break;
			}
		}
		else
		{
			cout<<"tr: extra operand ‘"<<argv[i]<<"’\ntry 'tr --help' for more information.\n";
			return 0;
		}
	}

	/*Based on my flags and sets we would perform different operations*/

	

	/*Set Manipulation & Expansion : It handles :digits: :space: etc as well*/
	if(k==0){
		cout<<"tr: missing operand\nTry 'tr --help' for more information.\n";
		return 0;
	}
	sets=k;
	char eset[2][128];
	while(--k>-1){
		set_manipulation(set[k]);
		expand(set[k],eset[k]);
	}
	
	
	/*complement logic*/
	
	if(flags[2]==1){
		
		complement(eset[0]);
		
	}
	
	/*Get File Descriptors*/
	int rfd,wfd;
	if(flags[3]==0)
		rfd=0;
	else{
		/*Open file Logic*/
		rfd=openfile(input_file,O_RDONLY);
		if(rfd==-1)
			return 0;
	}
	if(flags[4]==0)
		wfd=1;
	else{
		/*Open file Logic*/
		wfd=createfile(output_file,S_IRWXU);
	}

	/*delete logic*/
	
	if(flags[0]==1){
		if(sets==1){
			
			delete_char(eset[0]);
			while(read(rfd,buff,1)){
				c=buff[0];
				buff[0]=map_table[c];
				write(wfd,buff,1);
			}
		}
		else
		{
			cout<<"tr: one set is required for delete operation\n";
			return 0;
		}
	}


	/*squeeze logic*/
	if(flags[1]==1 ){
		if(sets==1){
			
			squeeze(rfd,wfd,eset[0]);
		}
		else
		{
			cout<<"tr: one set is required for squeeze operation\n";
			return 0;
		}
	}

	/*translation logic*/
	
	if(flags[0]==0 && flags[1]==0){
		if(sets==2){
			map(eset[0],eset[1]);
			while(read(rfd,buff,1)){
				c=buff[0];
				buff[0]=map_table[c];
				write(wfd,buff,1);
			}
			close(rfd);
		}
		else
		{
			cout<<"tr: two sets are required for Translation operation\n";
			return 0;
		}
		
	}
	free(buff);
	
	free(set);

		return 0;
}
