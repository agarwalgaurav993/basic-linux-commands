/*Gaurav Agarwal Roll - 20162088*/
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <dirent.h>
#include <malloc.h>
using namespace std;
#define MAX_SIZE 500;
DIR *sdirectory;

/* It would Create new files*/
int creatfile(char *path,char *filename,int permission)
{
	int i,k;
	char outfile[256];
	for(i=0,k=0;path[i]!='\0';i++,k++)
		outfile[k]=path[i];
	if(outfile[k-1]!='/')
		outfile[k++]='/';
	for(i=0;filename[i]!='\0';i++,k++)
		outfile[k]=filename[i];
	outfile[k]='\0';
	return creat(outfile,permission);
}

/*Check for access and will open file*/
int openfile(char *path,char *filename,int mode)
{
	int i,k;
	char outfile[256];
	for(i=0,k=0;path[i]!='\0';i++,k++)
		outfile[k]=path[i];
	if(outfile[k-1]!='/')
		outfile[k++]='/';
	for(i=0;filename[i]!='\0';i++,k++)
		outfile[k]=filename[i];
	outfile[k]='\0';
	int fd;
	int check=access(outfile, R_OK);
	if(check==0){
		fd=open(outfile,mode);
		if(fd==-1){
			cout<<outfile<<": No such file or directory\n";
			return fd;
		}
	}
	else
	{
		cout<<outfile<<": No Read permission\n";
		return check;
	}
	
	return fd;
}

/*Main implemenation: It would split and tac the file in hand*/

void fileCreation(char* ipath,char* opath,char* filename,char *lines)
{

	int rfd=openfile(ipath,filename,O_RDONLY);
	if(rfd==-1)
		return;
	int line=0;
	for(int i=0;lines[i]!='\0';i++)
		line=line*10+(lines[i]-'0');
	int count=0,c,i,k,mark,j;
	char *buff=(char*)malloc(sizeof(char));
	char sbuff[1];
	sbuff[0]=' ';
	char outfile[256];
	for(i=0,k=0;filename[i]!='\0';i++,k++){
		if(filename[i]=='.'){
			
			outfile[k++]='_';
			mark=k;
			outfile[k++]='1';

			
		}
		outfile[k]=filename[i];
	}
	outfile[k]='\0';

	int file_num=1;
	int wfd=creatfile(opath,outfile,S_IRWXU);
	
	for(j=1;j<line;j++)
		for(i=1;i<=500;i++)
		{
				write(wfd,sbuff,1);
		}
	lseek(wfd, (line-1 - count)*500, SEEK_SET);
	write(wfd,"\n",1);

	while(read(rfd,buff,1))
	{
		
		c=buff[0];
		if(c=='\n'){
			
			count++;
			
			
			if(count==line)
			{
				lseek(wfd, 0, SEEK_SET);
				close(wfd);
				if(read(rfd,buff,1)){
					outfile[mark]='0'+(++file_num);
	           	 	wfd=creatfile(opath,outfile,S_IRWXU);
	           	 	
	           	 	for(i=0;i<500;i++)
					{
						for(j=1;j<line;j++)
							write(wfd,sbuff,1);
					}
	           	 	count=0;
	           	 	lseek(wfd, (line-1 - count)*500, SEEK_SET);
	           	 	write(wfd,"\n",1);
					write(wfd,buff,1);
	           	 	
	           	}
	           	 else
	           	 	break;
			}
			else{
				write(wfd,'\0',1);
				lseek(wfd, (line-1- count)*500, SEEK_SET);
				if(count!=line-1)
					write(wfd,buff,1);
			}
			
		}
		else
		{
			write(wfd, buff, 1);
		}
	}
	close(rfd);
	close(wfd);
}
/**/

void makeDirectory(char* path)
{
	int i;
	for(i=0;path[i]!='\0';i++)
	{
		if(path[i]=='/')
		{
			
			path[i]='\0';
			
				mkdir(path, S_IRWXU);
			
			path[i]='/';
		}
	}
	mkdir(path, S_IRWXU);

}
/**/

void printfile(char* path,char*filename){
	char outfile[256];
	int i,k;
	for(i=0,k=0;path[i]!='\0';i++,k++)
		outfile[k]=path[i];
	if(outfile[k-1]!='/')
		outfile[k++]='/';
	for(i=0;filename[i]!='\0';i++,k++)
		outfile[k]=filename[i];
	outfile[k]='\0';
	cout<<outfile<<"\n";
}

/*Driver*/

int main(int argc,char *argv[])
{
	int len;
	if(argc!=4){
		cout<<"Arguments not Proper.\n";
		return 0;
	}

	makeDirectory(argv[2]);
	sdirectory=opendir(argv[1]);
	if(sdirectory == NULL){
		cout<<"Directory do not Exists.\n";
        return -1;
    }
    struct dirent *file;

    while((file=readdir(sdirectory))!= NULL)
    {
    	len=0;
    	while(file->d_name[len++]);
    	if(len>4)
    		if(file->d_name[len-2] == 't' && file->d_name[len-3] == 'x' && file->d_name[len-4] == 't')
    			fileCreation(argv[1],argv[2],file->d_name,argv[3]);
    }
	closedir(sdirectory);
	return 0;
}
