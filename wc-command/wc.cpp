/*Gaurav Agarwal, Roll - 20162088*/
#include<iostream>
#include<malloc.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/uio.h>
#include<sys/stat.h>
#define IN 1 /*inside a word*/
#define OUT 0 /*outside a word*/
using namespace std;
int ar[5];

/*It counts word, characters, longst line, lines*/
int openfile(char *filename,int mode)
{
	
	int fd;
	int check=access(filename, R_OK);
	if(check==0){
		fd=open(filename,mode);
		if(fd==-1){
			cout<<"wc: "<<filename<<": No Read permission\n";
			return fd;
		}
	}
	else
	{
		cout<<"wc: "<<filename<<": No such file or directory\n";
		return check;
	}
	
	return fd;
}

int wcount(char *filename)
{
	int fd=openfile(filename,O_RDONLY);
	if(fd==-1){
		return 0;
	}
	char *buff=(char*)malloc(sizeof(char));
	
	int state=OUT;
	int c,nl,nw,nc,nt;
	int L=0;
	nl=nw=nc=nt=0;
	
	while(read(fd,buff,1))
	{
		
		c=buff[0];
		nc++;
		nt++;
		if(c=='\n'){
			if(L<nt){
				L=nt-1;
				
			}	
			nt=0;	
			nl++;
		}
		if(c==' '||c=='\n'||c=='\t')
			state=OUT;
		else if(state==OUT)
		{
			state=IN;
			nw++;
		}
	}
	close(fd);
	ar[0]=nl;
	ar[1]=nw;
	ar[2]=nc;
	ar[3]=nc;
	ar[4]=L;	
	return 1;

}

int main(int argc,char *argv[]){
	int light[5]={0};
	int flag=0;
	char* c;
	
	if(argc==2)
	{
		
		flag=wcount(argv[1]);
		if(flag==0)
			return 0;
		cout<<" "<<ar[0]<<" "<<ar[1]<<" "<<ar[2]<<" "<<argv[1]<<"\n";
		return 0;

	}
	
	if(argc>2)
	{
		while(argc-->1)
		{
			int i=1;
			if(argv[argc][0]!='-')
                        {
				c=argv[argc];
                                flag=wcount(argv[argc]);
				if(flag==0)
				{
					return 0;
				}
                        }

			if(argv[argc][0]=='-')
			{
				while(argv[argc][i]!='\0')
				{
				switch(argv[argc][i++])
				{
					case 'l': light[0]=1;
						break;
			 		case 'c': light[3]=1;
        	                                break;
			 		case 'm': light[2]=1;
                	                        break;
			 		case 'w': light[1]=1;
                        	                break;
			 		case 'L': light[4]=1;
                                	        break;
					default:
						cout<<"wc: invalid option -- '"<<argv[argc][i-1]<<"'\n";
						return 0;
				}
				}
			}
		}
	}	
	if(flag==1){
	for(int i=0;i<5;i++)
	{
		if(light[i]==1)
			cout<<" "<<ar[i];
	}
	cout<<"\t"<<c;
	cout<<"\n";
	}
	return 0;
}
